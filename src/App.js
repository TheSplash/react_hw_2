import './App.css';
import Navbar from './components/Navbar';
import Shop from './components/Shop';

import { useEffect, useState } from 'react';

function App() {
  const initialCart = JSON.parse(localStorage.getItem('cart')) || [];
  const [cart, setCart] = useState(initialCart)
  const [amount, setAmount] = useState([])
  const [info, setInfo] = useState(false)
  const [favorites, setFavorites] = useState([])

  /* FAVOURITES */

  const toggleFavourites = (id) => {
    if (favorites.includes(id)) {
      const updatedFavorites = favorites.filter((el) => id !== el);
      setFavorites(updatedFavorites);
      localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
    } else {
      const updatedFavorites = [...favorites, id];
      setFavorites(updatedFavorites);
      localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
    }
    console.log(favorites.length);
  };

  useEffect(() => {
    const storedFavorites = JSON.parse(localStorage.getItem('favorites'));
    if (storedFavorites) {
      setFavorites(storedFavorites);
    }
  }, []);

  /* AJAX */

  useEffect(() => {
    fetch('./data.json')
      .then((response) => {
        return response.json();
      })
        .then((data) => {
          setAmount(data)
        });
    
  }, [])

  /* CART */

  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cart));
  }, [cart]);

  const handleClick = (item) => {
    const isPresent = cart.some((product) => item.id === product.id);

    if (isPresent) {
      setInfo(true);
      setTimeout(() => {
        setInfo(false);
      }, 2000);
      return;
    }
    setCart([...cart, item]);
  };

  return (
    <div className="App">
      <Navbar size={cart.length} favouritesSize={favorites.length}/>
      {
        info &&  <div className='info-block'>This item was added before!</div> 
      }
      <Shop handleClick={handleClick} cart={cart} amount={amount} toggleFavourites={toggleFavourites}/> 
    </div>
  );
}

export default App;
