import { useState } from 'react'
import React from 'react'
import Button from './Button'
import Modal from './Modal';
import '../style/cards.scss'

const Cards = ({item, handleClick, toggleFavourites}) => {
  const {name, image_link, price, id} = item
  const [isModalOpen, setIsModalOpen] = useState(false)
  const [choosen, setChoosen] = useState(false)

  const toggleChoose = () => {
    setChoosen(!choosen)
  }

  const starBackground = choosen 
  ?{backgroundColor: "#66849a"}
  :{backgroundColor: "white"}

  const toggleFirst = () => {
    setIsModalOpen(!isModalOpen);
  }

  const modalActions = (
    <>
      <Button text="Ok" className='modal-buttons__item' onClick={() => {handleClick(item); toggleFirst()}}/>
      <Button text="Cancel" className='modal-buttons__item' onClick={toggleFirst} />
    </>
  )

  return (
    <div className="cards">
        <div className="image_box">
            <img src={image_link} alt="img" />
        </div>

        <div className="details">
            <p>{name}</p>
            <p>Price - {price}</p>
            <Button text="Add to cart" backgroundColor="#66849a" onClick={toggleFirst} />
            <button className='details__button' style={starBackground} onClick={() => {toggleChoose(); toggleFavourites(id)}}></button>

            {
              isModalOpen &&
              <Modal 
                onClose={toggleFirst} 
                header="Do you want to add this book to cart ?" 
                text="You can see number of orders in header"
                actions={modalActions}
              />
            }
        </div>
    </div>
  )
}

export default Cards