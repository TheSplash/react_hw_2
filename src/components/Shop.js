import React from 'react'
import Cards from './Cards'
import '../style/shop.scss'

const Shop = ({handleClick, cart, amount, toggleFavourites}) => {
  return (
    <section className="shop_container">
        {
            amount.map((item) => (
               <Cards item={item} key={item.id} handleClick={handleClick} toggleFavourites={toggleFavourites}/>
            ))
        }
    </section>
  )
}

export default Shop

