import React from 'react'
import cart from './cart-shopping-solid.svg'
import star from './star.svg'
import '../style/navbar.scss'

const Navbar = ({size, favouritesSize}) => {
  return (
    <nav className='nav'>
        <div className='nav-box'>
            <div className='nav_cart-container'>
              <span className='nav_cart-title'>
                My Shopping
              </span>

              <span>
                <img src={cart} alt="cart"/>
                {size}
              </span>
            </div>

            <div className='nav_favourites-container'>
              <span>
                My Favourites
              </span>

              <span>
                <img src={star} alt='star' className='nav_star-img'></img>
                {favouritesSize}
              </span>
            </div>
        </div>
    </nav>
  )
}

export default Navbar